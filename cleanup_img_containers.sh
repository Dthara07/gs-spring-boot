# This script is to clean up all the images and containers locally
echo "---------Cleanup of Images Start -----------"
docker rmi $(docker images -aq) -f
echo "---------Cleanup of Images End -----------"
echo "---------Cleanup of Container Start -----------"
docker rm $(docker ps -aq)
echo "---------Cleanup of Container End -----------"
